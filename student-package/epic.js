/**
 * @author Danny Hoang
 * @since 2023-11-15
 * EPIC.js to load photos from a api to display earth at different times and dates.
 */

"use strict";
!(function () {

  const IMAGE_TYPES = ["natural", "enhanced", "aerosol", "cloud",];  
  let EpicApplication = {
    imagescache: {},
    datecache: {},
    timecache: {},
  };
  
  document.addEventListener("DOMContentLoaded", setup);
   /**
   * A function that adds a description sourced from the object data to date and description. 
   * @param {*} obj 
   * @param {*} dateDescription 
   * @param {*} titleDescription 
   */
  function appendDescription(obj,dateDescription, titleDescription){
    //dateDescription.textContent = "";
    //titleDescription.textContent = "";
    dateDescription.textContent = obj.date;
    titleDescription.textContent = obj.caption;
  }
   /**
   * A function that clones the amount of li required from data list created from the object, which will be appended to 
   * the Unordered list.
   * @param {obj} obj 
   * @param {HTMLElement} liTemplate 
   * @param {HTMLElement} orderlist 
   * @param {HTMLTemplateElement} li 
   * @param {HTMLElement} ul 
   */
  function appendList(EpicApplication, obj, liTemplate, orderlist, li, ul,date,type) {
    //create the list if the date is
    if (li.length > 0) {
      ul.textContent = "";
    } 
    //use the cache to create the list
    if (EpicApplication.timecache[type].has(date)) {
      let obj = EpicApplication.timecache[type].get(date);
      obj.forEach((time) => {

        let li = liTemplate.content.cloneNode(true);
        li.querySelector("span").textContent = time;
        orderlist.appendChild(li);
      });
      return;
    }

    //create the list if the date is not in the cache
    obj.forEach((time) => {
      time = time.date;
      let li = liTemplate.content.cloneNode(true);
      li.querySelector("span").textContent = time;
      orderlist.appendChild(li);
    });
    let timeArray = obj.map((time) => {
      return time.date; 
    });
    EpicApplication.timecache[type].set(date,timeArray);
  }
  /**
   * A function that creates a list of urls based on the return object data.
   * @param {object} obj 
   * @param {String} type 
   * @param {date} date 
   * @param {EpicApplication} EpicApplication
   * @returns A array of urls as STRINGS.
   */
  function createListOfUrls(obj, type, date, EpicApplication) {
    // Check if the object contains a date already for the day
    if (EpicApplication.imagescache[type] === null) {
      EpicApplication.imagescache[type] = new Map();
    } else if (EpicApplication.imagescache[type].has(date.toString())) {
      return EpicApplication.imagescache[type];
    }

    // If it doesn't, then create a list of URLs and a day for those URLs to be stored in the object
    EpicApplication.imagescache[type].set(date.toString());
    //make a map with a date as a key and a list of urls as a value
    let datearray = date.split("-");
    EpicApplication.imagescache[type].set(date,obj.map
      ((url) => {
        return `https://epic.gsfc.nasa.gov/archive/${type}/${datearray[0]}/${datearray[1]}/${datearray[2]}/jpg/${url.image}.jpg`;
      })
    );
  }


   /**
   * Function that takes all the lis and adds a index to each of them.
   * @param {HTMLElement} listofLi 
   */
  function setIndexes(listofLi){
    let i = 0;
    listofLi.forEach((li) => {
      li.setAttribute("data-image-list-index", i);
      i++;
    });
  }

   /**
   * Function that query for the max date for a type and sets the max date for date element.
   * @param {String} type  
   * @param {HTMLElement} dateInput
   * @param {EpicApplication} EpicApplication 
   */
  function maxDate(type, dateInput, EpicApplication) {
    window.EpicApplication = EpicApplication;
    //check if the object has a date already for the type 
    //if it does then fetch the max date and set the max date for the date input and store it in the object
    //check if epicApplication is empty
    if(EpicApplication.datecache[type] != null){
      dateInput.setAttribute("max", EpicApplication.datecache[type].toString());
    }
    else{
     fetch(`https://epic.gsfc.nasa.gov/api/${type}/all`)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response could not find max date ");
            }
            return response.json();
        })
        .then((obj) =>{
          //store the max date in the object
            let dateDescOrder = obj.map(object =>object.date.toString());
            dateDescOrder.sort((a,b) => new Date(b)- new Date(a));
            let maxDate = dateDescOrder[0];
            dateInput.setAttribute("max", maxDate);
            //cache it in the object
            EpicApplication.datecache[type] = maxDate;      
             
        })
        .catch((error) => {
            console.error("Error fetching data:", error);
            ul.textContent = "Error there is no data for this day try another day.";
            throw error; // Re-throw the error to propagate it through the chain
        });
      }
}
 /**
   * Function that takes all the required html elements and values to
   *  create the list for the user to click to see pictures of earth
   * @param {String} type
   * @param {date} date  
   * @param {HTMLElement} image 
   * @param {HTMLElement} orderlist 
   * @param {HTMLElement} liTemplate 
   * @param {HTMLElement} ul 
   * @param {HTMLElement} dateDescription 
   * @param {HTMLElement} titleDescription 
   */
  function makelist(type, date, image, orderlist, liTemplate, ul,dateDescription,titleDescription) {
    ul.textContent ="";
      //if epicApplication is not empty at this date then fetch the list of urls and create the list
      //if it is empty then fetch the list of urls and create the list
    if (EpicApplication.imagescache[type] != null) {
      if (EpicApplication.imagescache[type].has(date.toString())) {
        let listOfUrls = EpicApplication.imagescache[type].get(date.toString());
        //create a list of li
        let li = document.querySelectorAll("li");
        appendList(EpicApplication, listOfUrls, liTemplate, orderlist, li, ul,date,type);
        //each li has a index
        let listOfli = document.querySelectorAll("li");
        setIndexes(listOfli);
        //click event
        ul.addEventListener("click", function (e) {
          if (e.target.tagName == "SPAN") {
            let index = e.target.parentNode.getAttribute(
              "data-image-list-index"
            );
            image.src = EpicApplication.imagescache[type].get(date)[index];
            appendDescription(obj[index],dateDescription,titleDescription);
          }
        });
        return;
      }
      //do fetch fetch the list of urls and create the list
    fetch(`https://epic.gsfc.nasa.gov/api/${type}/date/${date}`)
      .then((response) => {
        // Stage 1: Take in a response and verify if it's OK
        console.log("1) Response object:", response);

        if (!response.ok) {
          throw new Error("Not 2xx response", { cause: response });
        }

        // If the response is OK, deserialize it.
        return response.json();
      })

      .then((obj) => {
        let listOfUrls = "";
        // Stage 2: Take the de-serialized response and do something with it
        console.log("2) Deserialized response:", obj);
        if(obj.length < 1){
          ul.textContent = "No data was found";
          return;
        }
    
        //obj sorted desc.
        obj = obj.reverse();
        //list of URLS
        

         //listOfUrls = createListOfUrls(obj, type, date, EpicApplication);
        createListOfUrls(obj, type, date, EpicApplication);
        //create a list of li
        let li = document.querySelectorAll("li");
        appendList(EpicApplication, obj, liTemplate, orderlist, li, ul,date,type);
        //each li has a index
        let listOfli = document.querySelectorAll("li");
        setIndexes(listOfli);
        //click event
        ul.addEventListener("click", function (e) {
          if (e.target.tagName == "SPAN") {
            let index = e.target.parentNode.getAttribute(
              "data-image-list-index"
            );
            image.src = EpicApplication.imagescache[type].get(date)[index];
            appendDescription(obj[index],dateDescription,titleDescription);
          }
        });
      })
      .catch((err) => {
        // Stage 3: Handle any errors
        console.error("3)Error:", err);
        ul.textContent = "Error there is no data for this day try another day.";
      });
  }
}

  /**
   * A function that setups all the javascript once the domcontent is loaded
   */
  function setup() {
    //get select element
    const select = document.getElementById("type");
    IMAGE_TYPES.forEach((type) => {
      EpicApplication.imagescache[type] = new Map();
      EpicApplication.datecache[type] = null;
      EpicApplication.timecache[type] = new Map();
    });
    IMAGE_TYPES.forEach((type) => {
      // Create a new option element
      let option = document.createElement("option");
      option.value = type;
      //make the first letter capital
      type = type.charAt(0).toUpperCase() + type.slice(1);
      option.text = type;
    
      // Append the option to the select element
      select.appendChild(option);
    });
     
    const form = document.getElementById("request_form");
    let type = document.getElementById("type").value;
    let dateInput = document.getElementById("date");
    const dateDescription= document.getElementById("earth-image-date");
    const titleDescription = document.getElementById("earth-image-title");
    maxDate(type,dateInput, EpicApplication);

    form.addEventListener("submit", function (e) {
      type = document.getElementById("type").value;
      let date = document.getElementById("date").value;
      const image = document.getElementById("earth-image");
      const orderlist = document.getElementById("image-menu");
      const liTemplate = document.getElementById("image-menu-item");
      let ul = document.getElementById("image-menu");
      e.preventDefault();
      makelist(type, date, image, orderlist, liTemplate, ul,dateDescription,titleDescription );
    });
        // When user changes the type it will refresh the list and available dates
    select.addEventListener("change", function (e) {
      let type = document.getElementById("type").value;
      let date = document.getElementById("date").value;
      const image = document.getElementById("earth-image");
      const orderlist = document.getElementById("image-menu");
      const liTemplate = document.getElementById("image-menu-item");
      let ul = document.getElementById("image-menu");
      maxDate(type, dateInput, EpicApplication);
      makelist(type,date,image,orderlist,liTemplate,ul);
    });
  }
})();
